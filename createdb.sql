-- Create tables

--- Movies
CREATE TABLE movies(
	ID INT NOT NULL,
	title VARCHAR(250) NOT NULL,
	imdbID VARCHAR(7) NOT NULL,
	spanishTitle VARCHAR(250),
	imdbPictureURL VARCHAR(300),
	year SMALLINT NOT NULL,
	rtID VARCHAR(100),
	rtAllCriticsRating SMALLINT,
	rtAllCriticsNumReviews SMALLINT,
	rtAllCriticsNumFresh SMALLINT,
	rtAllCriticsNumRotten SMALLINT,
	rtAllCriticsScore SMALLINT,
	rtTopCriticsRating NUMBER(2),
	rtTopCriticsNumReviews SMALLINT,
	rtTopCriticsNumFresh SMALLINT,
	rtTopCriticsNumRotten SMALLINT,
	rtTopCriticsScore SMALLINT,
	rtAudienceRating NUMBER(2),
	rtAudienceNumRatings SMALLINT,
	rtAudienceScore SMALLINT,
	rtPictureURL VARCHAR(300),
	PRIMARY KEY(ID)
);

CREATE TABLE movie_genres(
	movieID INT NOT NULL,
	genre VARCHAR(60) NOT NULL,
	FOREIGN KEY(movieID) REFERENCES Movies(ID) ON DELETE CASCADE
);

CREATE TABLE movie_directors(
	movieID INT NOT NULL,
	directorID VARCHAR(60) NOT NULL,
	directorName VARCHAR(60) NOT NULL,
	PRIMARY KEY(directorID, movieID),
	FOREIGN KEY(movieID) REFERENCES Movies(ID) ON DELETE CASCADE
);

CREATE TABLE movie_actors(
	movieID INT NOT NULL,
	actorID VARCHAR(60) NOT NULL,
	actorName VARCHAR(60),
	ranking SMALLINT NOT NULL,
	PRIMARY KEY(actorID, movieID),
	FOREIGN KEY(movieID) REFERENCES Movies(ID) ON DELETE CASCADE
);

CREATE TABLE movie_countries(
	movieID INT NOT NULL,
	country VARCHAR(50),
	FOREIGN KEY(movieID) REFERENCES Movies(ID) ON DELETE CASCADE
);

CREATE TABLE movie_locations(
	movieID INT NOT NULL,
	location1 VARCHAR(25),
	location2 VARCHAR(50),
	location3 VARCHAR(100),
	location4 VARCHAR(200),
	FOREIGN KEY(movieID) REFERENCES Movies(ID) ON DELETE CASCADE
);

--- Tags
CREATE TABLE tags(
	ID INT NOT NULL,
	value VARCHAR(50) NOT NULL,
	PRIMARY KEY(ID)
);

CREATE TABLE movie_tags(
	movieID INT NOT NULL,
	tagID INT NOT NULL,
	tagWeight SMALLINT NOT NULL,
	FOREIGN KEY(tagID) REFERENCES Tags(ID) ON DELETE CASCADE,
	FOREIGN KEY(movieID) REFERENCES Movies(ID) ON DELETE CASCADE
);

--- Users
---- Tags
CREATE TABLE user_taggedmovies(
	userID INT NOT NULL,
	movieID INT NOT NULL,
	tagID INT NOT NULL,
	date_day SMALLINT NOT NULL,
	date_month SMALLINT NOT NULL,
	date_year SMALLINT NOT NULL,
	date_hour SMALLINT NOT NULL,
	date_minute SMALLINT NOT NULL,
	date_second SMALLINT NOT NULL,
	FOREIGN KEY(tagID) REFERENCES Tags(ID) ON DELETE CASCADE,
	FOREIGN KEY(movieID) REFERENCES Movies(ID) ON DELETE CASCADE
);

CREATE TABLE user_taggedmovies_timestamps(
	userID INT NOT NULL,
	movieID INT NOT NULL,
	tagID INT NOT NULL,
	timestamp NUMBER NOT NULL,
	FOREIGN KEY(tagID) REFERENCES Tags(ID) ON DELETE CASCADE,
	FOREIGN KEY(movieID) REFERENCES Movies(ID) ON DELETE CASCADE
);

---- Ratings
CREATE TABLE user_ratedmovies(
	userID INT NOT NULL,
	movieID INT NOT NULL,
	rating SMALLINT NOT NULL,
	date_day SMALLINT NOT NULL,
	date_month SMALLINT NOT NULL,
	date_year SMALLINT NOT NULL,
	date_hour SMALLINT NOT NULL,
	date_minute SMALLINT NOT NULL,
	date_second SMALLINT NOT NULL,
	FOREIGN KEY(movieID) REFERENCES Movies(ID) ON DELETE CASCADE
);

CREATE TABLE user_ratedmovies_timestamps(
	userID INT NOT NULL,
	movieID INT NOT NULL,
	rating SMALLINT NOT NULL,
	timestamp NUMBER NOT NULL,
	FOREIGN KEY(movieID) REFERENCES Movies(ID) ON DELETE CASCADE
);

-- Create Indices

CREATE INDEX idx_genre ON movie_genres(genre);
CREATE INDEX idx_movie_year ON movies(year);
CREATE INDEX idx_movie_country ON movie_countries(country);
CREATE INDEX idx_movie_actors ON movie_actors(actorName);
CREATE INDEX idx_tags_id ON movie_tags(tagID);
CREATE INDEX idx_tags_weight ON movie_tags(tagWeight);