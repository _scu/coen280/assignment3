-- Drop Indices

DROP INDEX idx_tags_weight;
DROP INDEX idx_tags_id;
DROP INDEX idx_movie_actors;
DROP INDEX idx_movie_country;
DROP INDEX idx_movie_year;
DROP INDEX idx_genre;

-- Drop Tables

DROP TABLE user_ratedmovies_timestamps;
DROP TABLE user_ratedmovies;

DROP TABLE user_taggedmovies_timestamps;
DROP TABLE user_taggedmovies;

DROP TABLE movie_tags;
DROP TABLE tags;

DROP TABLE movie_locations;
DROP TABLE movie_countries;
DROP TABLE movie_actors;
DROP TABLE movie_directors;
DROP TABLE movie_genres;
DROP TABLE movies;