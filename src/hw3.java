import oracle.jdbc.OracleDriver;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.List;
import java.util.concurrent.Callable;

public class hw3 extends JPanel {
    private SQLUtility sqlUtility = new SQLUtility();

    private DefaultListModel<JCheckBox> genreModel = new DefaultListModel<>();
    private DefaultListModel<JCheckBox> countryModel = new DefaultListModel<>();
    private DefaultListModel<JCheckBox> actorModel = new DefaultListModel<>();

    private JSpinner startJSpinner = new JSpinner();
    private JSpinner endJSpinner = new JSpinner();

    private List<JComboBox<String>> actorsQuery = new ArrayList<>();

    private JComboBox<String> directorComboBox = new JComboBox<>();
    private JComboBox<String> tagsWeightOperation = new JComboBox<>();
    private JComboBox<String> and_or = new JComboBox<>();

    private JTextField actorsFilter = new JTextField();
    private JTextField tagsWeight = new JTextField();

    private JTable tagsTable = new JTable();
    private JTable movieResultTable = new JTable();
    private JTable userResultTable = new JTable();

    private ResultTableModel tagsTableModel;
    private ResultTableModel movieTableModel;
    private ResultTableModel userTableModel;

    private JTextArea movieQueryDebugOutput = new JTextArea(5, 20);
    private JTextArea userQueryDebugOutput = new JTextArea(5, 20);

    private ChangeListener requisitesChangeListener;
    private ChangeListener countryChangeListener;
    private ChangeListener actorChangeListener;

    private ActionListener castActionListener;

    private boolean isTest = true;

    private Color defaultColor = new Color(0, 128, 255);

    public hw3() {
        initLayout();

        populateGenre();
        populateYears();

        populateMoviesTable();
    }

    private void initLayout() {
        setLayout(new BorderLayout());
        add(initHeaderPanel(), BorderLayout.NORTH);
        add(initDashboardPanel(), BorderLayout.CENTER);
        add(initQueryViewPanel(), BorderLayout.SOUTH);
    }

    private JPanel initHeaderPanel() {
        JPanel headerPanel = customPanel(new BorderLayout());

        JPanel inputPanel = customPanel(new GridLayout(1, 0));

        JTextField sqlHost = new JTextField("localhost");
        JTextField sqlPort = new JTextField("1521");

        JTextField sqlDatabase = new JTextField("ORCLCDB.localdomain");

        JTextField sqlUsername = new JTextField("mhalinge");
        JPasswordField sqlPassword = new JPasswordField("password");

        inputPanel.add(componentWithLabel("Host", sqlHost));
        inputPanel.add(componentWithLabel("Port", sqlPort));

        inputPanel.add(componentWithLabel("Database", sqlDatabase));

        inputPanel.add(componentWithLabel("ID", sqlUsername));
        inputPanel.add(componentWithLabel("Password", sqlPassword));

        JButton connectButton = new JButton("Connect");

        connectButton.addActionListener((ActionEvent e) -> {
                try {
                    sqlUtility.openConnection(
                            sqlHost.getText().trim(),
                            sqlPort.getText().trim(),
                            sqlDatabase.getText().trim(),
                            sqlUsername.getText().trim(),
                            new String(sqlPassword.getPassword()));

                    populateGenre();
                    populateYears();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
        });
        headerPanel.add(inputPanel, BorderLayout.CENTER);
        headerPanel.add(connectButton, BorderLayout.SOUTH);

        return this.panelWithHeader("Connection", headerPanel, defaultColor.darker());
    }

    private JPanel initDashboardPanel() {
        JPanel dashboardPanel = customPanel(new GridLayout(2, 0));
        dashboardPanel.add(initAttributesPanel());
        dashboardPanel.add(initResultViewPanel());

        return dashboardPanel;
    }

    private JPanel initAttributesPanel() {
        JPanel attributesParentPanel = customPanel(new BorderLayout(2, 2));

        JPanel attributesPanel = customPanel(new GridLayout(1, 0, 2, 2));

        attributesPanel.add(initRequisitesPanel());
        attributesPanel.add(initCountryPanel());
        attributesPanel.add(initCastPanel());
        attributesPanel.add(initTagValueViewPanel());

        and_or.addItem(" OR ");
        and_or.addItem(" AND ");

        and_or.setSelectedIndex(0);

        and_or.addActionListener(e -> {
            populateMoviesTable();
        });

        attributesParentPanel.add(attributesPanel, BorderLayout.CENTER);
        attributesParentPanel.add(componentWithLabel("Select AND/OR between attributes", and_or), BorderLayout.SOUTH);

        return this.panelWithHeader("Movie Attributes", attributesParentPanel, defaultColor.darker());
    }

    private JPanel initRequisitesPanel() {
        JPanel requisitesPanel = customPanel(new BorderLayout());

        requisitesPanel.add(initGenrePanel(), BorderLayout.CENTER);
        requisitesPanel.add(initYearsPanel(), BorderLayout.SOUTH);

        requisitesChangeListener = (e) -> {
            populateCountries();
            populateActors(actorsFilter.getText().trim());
            populateDirectors();
            populateTagsTable();
            populateMoviesTable();
        };

        return requisitesPanel;
    }

    private JPanel initGenrePanel() {
        return scrollableListPanel("Genre", new JCheckBoxList(genreModel));
    }

    private JPanel initYearsPanel() {
        JPanel yearsPanel = customPanel(new BorderLayout());

        startJSpinner = new JSpinner();
        endJSpinner = new JSpinner();

        startJSpinner.setModel(new SpinnerDateModel());
        endJSpinner.setModel(new SpinnerDateModel());

        startJSpinner.setEditor(new JSpinner.DateEditor(startJSpinner, "yyyy"));
        endJSpinner.setEditor(new JSpinner.DateEditor(endJSpinner, "yyyy"));

        yearsPanel.add(componentWithLabel("From", startJSpinner), BorderLayout.NORTH);
        yearsPanel.add(componentWithLabel("To", endJSpinner), BorderLayout.SOUTH);

        return scrollableListPanel("Years", yearsPanel);
    }

    private JPanel initCountryPanel() {
        countryChangeListener = (e) -> {
            populateActors(actorsFilter.getText().trim());
            populateDirectors();
            populateTagsTable();
            populateMoviesTable();
        };
        return scrollableListPanel("Country", new JCheckBoxList(countryModel));
    }

    private JPanel initCastPanel() {
        JPanel castPanel = customPanel(new BorderLayout());

        castPanel.add(initActorsPanel(), BorderLayout.CENTER);
        castPanel.add(initDirectorPanel(), BorderLayout.SOUTH);

        castActionListener = (e) -> {
            populateTagsTable();
            populateMoviesTable();
        };

        actorChangeListener = (e) -> {
            populateTagsTable();
            populateMoviesTable();
        };

        return castPanel;
    }

    private JPanel initActorsPanel() {
        JPanel actorsPanel;
        if(isTest) {
            actorsPanel = new JPanel(new BorderLayout());

            actorsFilter.addKeyListener(new KeyListener() {
                @Override
                public void keyTyped(KeyEvent e) {

                }

                @Override
                public void keyPressed(KeyEvent e) {

                }

                @Override
                public void keyReleased(KeyEvent e) {
                    if(10 == e.getKeyCode()) {
                        populateActors(((JTextField) e.getSource()).getText().trim());
                    }
                }
            });

            actorsPanel.add(componentWithLabel("Filter", actorsFilter), BorderLayout.NORTH);
            actorsPanel.add(createScrollPane(new JCheckBoxList(actorModel)), BorderLayout.CENTER);
            return panelWithHeader("Cast", actorsPanel);
        } else {
            actorsPanel = new JPanel(new GridLayout(4, 0));
            for(int i = 0; i < 4; i++) {
                actorsQuery.add(new JComboBox<>());
                actorsPanel.add(actorsQuery.get(i));
            }
            return scrollableListPanel("Cast", actorsPanel);
        }
    }

    private JPanel initDirectorPanel() {
        return panelWithHeader("Director", directorComboBox);
    }

    private JPanel initTagValueViewPanel() {
        JPanel jPanel = new JPanel(new BorderLayout(1, 1));

        jPanel.add(initTagTablePanel(), BorderLayout.CENTER);
        jPanel.add(initTagWeightPanel(), BorderLayout.SOUTH);

        return jPanel;
    }

    private JPanel initTagTablePanel() {
        return scrollableListPanel("Tags", tagsTable);
    }

    private JPanel initTagWeightPanel() {
        JPanel tagsWeightPanel = new JPanel(new BorderLayout(1, 1));

        tagsWeightOperation.addItem(" > ");
        tagsWeightOperation.addItem(" < ");
        tagsWeightOperation.addItem(" >= ");
        tagsWeightOperation.addItem(" <= ");
        tagsWeightOperation.addItem(" = ");

        tagsWeightOperation.setSelectedIndex(0);

        tagsWeight = new JTextField("0");

        tagsWeightPanel.add(componentWithLabel("Operation", tagsWeightOperation), BorderLayout.NORTH);
        tagsWeightPanel.add(componentWithLabel("Value", tagsWeight), BorderLayout.SOUTH);

        actorsFilter.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                if(10 == e.getKeyCode()) {
                    populateMoviesTable();
                }
            }
        });
        return scrollableListPanel("Tags Weight", tagsWeightPanel);
    }

    private JPanel initResultViewPanel() {
        JPanel resultPanel = customPanel(new BorderLayout(1, 1));

        resultPanel.add(scrollableListPanel("Movies", movieResultTable), BorderLayout.CENTER);
        resultPanel.add(scrollableListPanel("Users", userResultTable), BorderLayout.EAST);

        return this.panelWithHeader("Results", resultPanel, defaultColor.darker());
    }

    private JPanel initQueryViewPanel() {
        JPanel queryPanel = customPanel(new GridLayout(1, 0, 1, 1));

        queryPanel.add(queryDebugPanel("Movie", movieQueryDebugOutput));
        queryPanel.add(queryDebugPanel("User", userQueryDebugOutput));

        return this.panelWithHeader("Queries", queryPanel, defaultColor.darker());
    }

    private JPanel queryDebugPanel(String label, JTextArea queryDebugOutput) {
//        queryDebugOutput.setFont(new Font(Font.DIALOG, Font.BOLD, 14));
        queryDebugOutput.setLineWrap(true);
        queryDebugOutput.setWrapStyleWord(true);

        queryDebugOutput.setEditable(false);

        return scrollableListPanel(label, queryDebugOutput);
    }

    private JLabel customLabel(String label, Color color) {
        JLabel jLabel = new JLabel(label, SwingConstants.CENTER);

        jLabel.setBorder(new EmptyBorder(2,0,2,0));
        jLabel.setOpaque(true);

        jLabel.setBackground(color);
        jLabel.setForeground(Color.WHITE);
        jLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 14));

        return jLabel;
    }

    private JPanel customPanel(LayoutManager layout) {
        JPanel jPanel = new JPanel(layout);

        jPanel.setBackground(Color.WHITE);

        return jPanel;
    }

    private JPanel componentWithLabel(String label, Component component) {
        JLabel jLabel = new JLabel(label);
        JPanel jPanel = new JPanel(new BorderLayout());
        jLabel.setLabelFor(jPanel);

        jPanel.add(jLabel, BorderLayout.WEST);
        jPanel.add(component, BorderLayout.CENTER);

        return jPanel;
    }

    private JScrollPane createScrollPane(Component component) {
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(component);

        return scrollPane;
    }

    private JPanel scrollableListPanel(String label, Component component) {
        return this.panelWithHeader(label, createScrollPane(component), this.defaultColor);
    }

    private JPanel panelWithHeader(String label, Component component) {
        return this.panelWithHeader(label, component, this.defaultColor);
    }

    private JPanel panelWithHeader(String label, Component component, Color headerColor) {
        JPanel panelWithHeader = customPanel(new BorderLayout(1, 1));

        panelWithHeader.add(customLabel(label, headerColor), BorderLayout.NORTH);
        panelWithHeader.add(component, BorderLayout.CENTER);

        return panelWithHeader;
    }

    private JCheckBox customJCheckBox(String name, String value, List<String> selectedList, ChangeListener changeListener) {
        JCheckBox jCheckBox = new JCheckBox();

        jCheckBox.setName(name);
        jCheckBox.setText(value);

        if(selectedList != null && selectedList.contains(name)) {
            jCheckBox.setSelected(true);
        }

        jCheckBox.addChangeListener(changeListener);

        return jCheckBox;
    }

    private void populateGenre() {
        genreModel.removeAllElements();

        QueryBuilder queryBuilder = new QueryBuilder();

        queryBuilder.target("genre")
                .distinct()
                .relation("movie_genres")
                .orderBy("genre");

        ResultSet resultSet = sqlUtility.query(queryBuilder.build());
        populateCheckBoxModel(resultSet, genreModel, null, 1, 1, requisitesChangeListener);
    }

    private void populateYears() {
        QueryBuilder queryBuilder = new QueryBuilder();

        queryBuilder.target("min(year)")
                .target("max(year)")
                .relation("movies");

        try {
            ResultSet resultSet = sqlUtility.query(queryBuilder.build());

            while(resultSet.next()) {
                Calendar calendar = new GregorianCalendar();

                calendar.set(Calendar.YEAR, resultSet.getInt(1));
                Date startDate = calendar.getTime();

                calendar.set(Calendar.YEAR, resultSet.getInt(2));
                Date endDate = calendar.getTime();

                SpinnerDateModel startYear = new SpinnerDateModel(startDate, null, null, Calendar.YEAR);
                SpinnerDateModel endYear = new SpinnerDateModel(endDate, null, null, Calendar.YEAR);

                startJSpinner.setModel(startYear);
                endJSpinner.setModel(endYear);

                startJSpinner.setEditor(new JSpinner.DateEditor(startJSpinner, "yyyy"));
                endJSpinner.setEditor(new JSpinner.DateEditor(endJSpinner, "yyyy"));

                startYear.addChangeListener(requisitesChangeListener);
                endYear.addChangeListener(requisitesChangeListener);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void populateCountries() {
        List<String> selectedCountryList = this.getSelectedValue(countryModel);

        countryModel.removeAllElements();

        List<String> selectedGenreList = this.getSelectedValue(genreModel);

        Integer startDate = this.getDateFromSpinner(startJSpinner);
        Integer endDate = this.getDateFromSpinner(endJSpinner);

        if(selectedGenreList.size() > 0 && startDate <= endDate) {
            QueryBuilder queryBuilder = new QueryBuilder();
            QueryBuilder genreQueryBuilder = new QueryBuilder();
            QueryBuilder yearQueryBuilder = new QueryBuilder();

            genreQueryBuilder.target("GM.movieID")
                    .distinct()
                    .relation("movie_genres GM")
                    .qualificationIN("GM.genre", selectedGenreList);

            yearQueryBuilder.target("MY.ID")
                    .distinct()
                    .relation("movies MY")
                    .qualification("MY.year", startDate, " >= ")
                    .qualification("MY.year", endDate, " <= ");

            queryBuilder.target("MC.country")
                    .distinct()
                    .relation("movie_countries MC")
                    .orderBy("MC.country");

            queryBuilder.qualificationIN("MC.movieID", genreQueryBuilder.build());
            queryBuilder.qualificationIN("MC.movieID", yearQueryBuilder.build());

            ResultSet resultSet = sqlUtility.query(queryBuilder.build());
            populateCheckBoxModel(resultSet, countryModel, selectedCountryList, 1, 1, countryChangeListener);

        }
    }

    private void populateActors(String actorsFilterText) {
        for(JComboBox<String> jComboBox: actorsQuery) {
            jComboBox.removeActionListener(castActionListener);
        }

        List<String> selectedGenreList = this.getSelectedValue(genreModel);
        List<String> selectedActorsList;

        if(isTest) {
            selectedActorsList = this.getSelectedValue(actorModel);
            actorModel.removeAllElements();
        } else {
            selectedActorsList = this.getSelectedValue(actorsQuery);
        }

        Integer startDate = this.getDateFromSpinner(startJSpinner);
        Integer endDate = this.getDateFromSpinner(endJSpinner);


        for(JComboBox<String> jComboBox: actorsQuery) {
            jComboBox.addItem(null);
        }

        if(selectedGenreList.size() > 0 && startDate <= endDate) {
            QueryBuilder queryBuilder = new QueryBuilder();
            QueryBuilder genreQueryBuilder = new QueryBuilder();
            QueryBuilder yearQueryBuilder = new QueryBuilder();

            genreQueryBuilder.target("GM.movieID")
                    .distinct()
                    .relation("movie_genres GM")
                    .qualificationIN("GM.genre", selectedGenreList);

            yearQueryBuilder.target("MY.ID")
                    .distinct()
                    .relation("movies MY")
                    .qualification("MY.year", startDate, " >= ")
                    .qualification("MY.year", endDate, " <= ");

            List<String> selectedCountryList = this.getSelectedValue(countryModel);

            queryBuilder.target("MA.actorName")
                    .distinct()
                    .relation("movie_actors MA")
                    .qualification("MA.actorName", actorsFilterText, " LIKE ")
                    .orderBy("MA.actorName");

            if(selectedCountryList.size() > 0) {
                QueryBuilder countryQueryBuilder = new QueryBuilder();
                countryQueryBuilder.target("MC.movieID")
                        .distinct()
                        .relation("movie_countries MC")
                        .qualificationIN("MC.country", selectedCountryList);

                queryBuilder.qualificationIN("MA.movieID", countryQueryBuilder.build());
            }

            queryBuilder.qualificationIN("MA.movieID", genreQueryBuilder.build());
            queryBuilder.qualificationIN("MA.movieID", yearQueryBuilder.build());

            try {
                ResultSet resultSet = sqlUtility.query(queryBuilder.build());

                if(isTest) {
//                    actorsFilter.addActionListener(actorsFilterActionListener);
                    populateCheckBoxModel(resultSet, actorModel, selectedActorsList, 1, 1, actorChangeListener);
                } else {
                    List<String> actorNameList = new ArrayList<>();

                    while(resultSet.next()) {
                        String item = resultSet.getString(1);
                        if(item != null && !item.isEmpty()) {
                            actorNameList.add(resultSet.getString(1));

                        }
                    }

                    actorsQuery.forEach(actorJComboBox -> {
                        actorNameList.forEach(actorName -> {
                            actorJComboBox.addItem(actorName);
                        });
                    });

                    for(JComboBox<String> jComboBox: actorsQuery) {
                        if(selectedActorsList != null && selectedActorsList.size() > 0) {
                            jComboBox.setSelectedItem(selectedActorsList.get(0));
                            selectedActorsList.remove(0);
                        }
                        jComboBox.addActionListener(castActionListener);
                    }
                }



            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    private void populateDirectors() {
        directorComboBox.removeActionListener(castActionListener);
        Object selectedDirector = directorComboBox.getSelectedItem();

        directorComboBox.removeAllItems();
        directorComboBox.addItem(null);

        List<String> selectedGenreList = this.getSelectedValue(genreModel);

        Integer startDate = this.getDateFromSpinner(startJSpinner);
        Integer endDate = this.getDateFromSpinner(endJSpinner);

        if(selectedGenreList.size() > 0 && startDate <= endDate) {
            QueryBuilder queryBuilder = new QueryBuilder();
            QueryBuilder genreQueryBuilder = new QueryBuilder();
            QueryBuilder yearQueryBuilder = new QueryBuilder();

            genreQueryBuilder.target("GM.movieID")
                    .distinct()
                    .relation("movie_genres GM")
                    .qualificationIN("GM.genre", selectedGenreList);

            yearQueryBuilder.target("MY.ID")
                    .distinct()
                    .relation("movies MY")
                    .qualification("MY.year", startDate, " >= ")
                    .qualification("MY.year", endDate, " <= ");

            List<String> selectedCountryList = this.getSelectedValue(countryModel);

            queryBuilder.target("MD.directorName")
                    .distinct()
                    .relation("movie_directors MD")
                    .orderBy("MD.directorName");

            if(selectedCountryList.size() > 0) {
                QueryBuilder countryQueryBuilder = new QueryBuilder();
                countryQueryBuilder.target("MC.movieID")
                        .distinct()
                        .relation("movie_countries MC")
                        .qualificationIN("MC.country", selectedCountryList);

                queryBuilder.qualificationIN("MD.movieID", countryQueryBuilder.build());
            }

            queryBuilder.qualificationIN("MD.movieID", genreQueryBuilder.build());
            queryBuilder.qualificationIN("MD.movieID", yearQueryBuilder.build());

            try {
                ResultSet resultSet = sqlUtility.query(queryBuilder.build());
                while(resultSet.next()) {
                    if(resultSet.getString(1) != null && !resultSet.getString(1).isEmpty()) {
                        directorComboBox.addItem(resultSet.getString(1));

                        if(resultSet.getString(1).equalsIgnoreCase("" + selectedDirector)) {
                            directorComboBox.setSelectedItem(resultSet.getString(1));
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            directorComboBox.addActionListener(castActionListener);
        }
    }

    private void populateTagsTable() {
        List<String> selectedGenreList = this.getSelectedValue(genreModel);

        Integer startDate = this.getDateFromSpinner(startJSpinner);
        Integer endDate = this.getDateFromSpinner(endJSpinner);

        ResultSet resultSet = null;

        if(selectedGenreList.size() > 0 && startDate <= endDate) {
            QueryBuilder queryBuilder = new QueryBuilder();

            QueryBuilder genreQueryBuilder = new QueryBuilder();
            QueryBuilder yearQueryBuilder = new QueryBuilder();
            QueryBuilder movieTagsQueryBuilder = new QueryBuilder();

            genreQueryBuilder.target("GM1.movieID")
                    .distinct()
                    .relation("movie_genres GM1")
                    .qualificationIN("GM1.genre", selectedGenreList);

            yearQueryBuilder.target("MY.ID")
                    .distinct()
                    .relation("movies MY")
                    .qualification("MY.year", startDate, " >= ")
                    .qualification("MY.year", endDate, " <= ");

            movieTagsQueryBuilder.target("MT.tagID")
                    .distinct()
                    .relation("movie_tags MT");

            List<String> selectedCountryList = this.getSelectedValue(countryModel);
            if(selectedCountryList.size() > 0) {
                QueryBuilder countryQueryBuilder = new QueryBuilder();
                countryQueryBuilder.target("MC.MovieID")
                        .distinct()
                        .relation("movie_countries MC")
                        .qualificationIN("MC.country", selectedCountryList);

                movieTagsQueryBuilder.qualificationIN("MT.movieID", countryQueryBuilder.build());
            }

            List<String> selectedActorsList;

            if(isTest) {
                selectedActorsList = getSelectedValue(actorModel);
            } else {
                selectedActorsList = getSelectedValue(actorsQuery);
            }

            if(selectedActorsList.size() > 0) {
                QueryBuilder actorsQueryBuilder = new QueryBuilder();
                actorsQueryBuilder.target("MA.MovieID")
                        .distinct()
                        .relation("movie_actors MA")
                        .qualificationIN("MA.actorName", selectedActorsList);

                movieTagsQueryBuilder.qualificationIN("MT.movieID", actorsQueryBuilder.build());
            }

            Object selectedDirector = directorComboBox.getSelectedItem();
            if(selectedDirector != null && !selectedDirector.toString().isEmpty()) {
                QueryBuilder directorQueryBuilder = new QueryBuilder();
                directorQueryBuilder.target("MD.MovieID")
                        .distinct()
                        .relation("movie_directors MD")
                        .qualification("MD.directorName", QueryBuilder.test(selectedDirector));

                movieTagsQueryBuilder.qualificationIN("MT.movieID", directorQueryBuilder.build());
            }

            movieTagsQueryBuilder.qualificationIN("MT.movieID", genreQueryBuilder.build());
            movieTagsQueryBuilder.qualificationIN("MT.movieID", yearQueryBuilder.build());

            queryBuilder
                    .target("T1.ID")
                    .target("T1.value")
                    .distinct()
                    .relation("tags T1")
                    .orderBy("T1.ID")
                    .qualificationIN("T1.ID", movieTagsQueryBuilder.build());

            resultSet = sqlUtility.query(queryBuilder.build());
        }

        tagsTableModel = new ResultTableModel(resultSet, () -> {
            this.populateMoviesTable();
            this.populateUsersTable();
            return 0;
        });

        tagsTable.setModel(tagsTableModel);
    }

    private List<String> getSelectedValue(DefaultListModel<JCheckBox> jCheckBoxDefaultListModel) {
        List<String> selectedJCheckBoxListItems = new ArrayList<>();
        for(int j = 0; j < jCheckBoxDefaultListModel.size(); j++) {
            if(jCheckBoxDefaultListModel.get(j).isSelected()) {
                selectedJCheckBoxListItems.add(jCheckBoxDefaultListModel.get(j).getName());
            }
        }
        return selectedJCheckBoxListItems;
    }

    private List<String> getSelectedValue(List<JComboBox<String>> jComboBoxList) {
        List<String> selectedJComboListItems = new ArrayList<>();
        for(JComboBox<String> jComboBox: jComboBoxList) {
            if(jComboBox.getSelectedItem() != null) {
                selectedJComboListItems.add(jComboBox.getSelectedItem().toString());
            }
        }

        return selectedJComboListItems;
    }

    private List<Object> getSelectedValue(ResultTableModel resultTableModel) {
        List<Object> selectedResultTableModelItems = new ArrayList<>();

        if(resultTableModel != null) {
            for(int i = 0; i < resultTableModel.getRowCount(); i++) {
                if(resultTableModel.getValueAt(i, 0) == Boolean.TRUE) {
                    selectedResultTableModelItems.add(resultTableModel.getValueAt(i, 1));
                }
            }
        }

        return selectedResultTableModelItems;
    }

    private Integer getDateFromSpinner(JSpinner jSpinner) {
        Calendar calendar = new GregorianCalendar();

        calendar.setTime((Date) jSpinner.getModel().getValue());
        return calendar.get(Calendar.YEAR);
    }

    private void populateCheckBoxModel(ResultSet resultSet, DefaultListModel<JCheckBox> listModel, List<String> selectedNameList, int name_index, int text_index, ChangeListener changeListener) {
        try {
            listModel.removeAllElements();
            while(resultSet.next()) {
                listModel.addElement(customJCheckBox(resultSet.getString(name_index), resultSet.getString(text_index), selectedNameList, changeListener));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void populateMoviesTable() {
        List<String> selectedGenreList = this.getSelectedValue(genreModel);

        Integer startDate = this.getDateFromSpinner(startJSpinner);
        Integer endDate = this.getDateFromSpinner(endJSpinner);

        boolean isAndOperation = " AND ".equals(and_or.getSelectedItem());

        ResultSet resultSet = null;
        if (selectedGenreList.size() > 0 && startDate <= endDate) {
            QueryBuilder queryBuilder = new QueryBuilder();

            QueryBuilder genreQueryBuilder = new QueryBuilder();
            genreQueryBuilder.target("GM.movieID")
                    .distinct()
                    .relation("movie_genres GM");

            QueryBuilder.andORQuery(isAndOperation, genreQueryBuilder, queryBuilder, selectedGenreList,
                    "M.ID", "GM.genre");

            List<String> selectedCountryList = this.getSelectedValue(countryModel);
            if (selectedCountryList.size() > 0) {
                QueryBuilder countryQueryBuilder = new QueryBuilder();
                countryQueryBuilder.target("CM.movieID")
                        .distinct()
                        .relation("movie_countries CM");

                QueryBuilder.andORQuery(isAndOperation, countryQueryBuilder, queryBuilder, selectedCountryList,
                        "M.ID", "CM.country");
            }

            List<String> selectedActorsList;

            if(isTest) {
                selectedActorsList = getSelectedValue(actorModel);
            } else {
                selectedActorsList = getSelectedValue(actorsQuery);
            }
            if (selectedActorsList.size() > 0) {
                QueryBuilder actorsQueryBuilder = new QueryBuilder();
                actorsQueryBuilder.target("AM.movieID")
                        .distinct()
                        .relation("movie_actors AM");

                QueryBuilder.andORQuery(isAndOperation, actorsQueryBuilder, queryBuilder, selectedActorsList,
                        "M.ID", "AM.actorName");
            }

            Object selectedDirector = directorComboBox.getSelectedItem();
            if (selectedDirector != null && !selectedDirector.toString().isEmpty()) {
                QueryBuilder directorQueryBuilder = new QueryBuilder();
                directorQueryBuilder.target("MD.movieID")
                        .distinct()
                        .relation("movie_directors MD")
                        .qualification("MD.directorName", QueryBuilder.test(selectedDirector));

                queryBuilder.qualificationIN("M.ID", directorQueryBuilder.build());
            }

            List<Object> selectedTagsValue = getSelectedValue(tagsTableModel);
            if(selectedTagsValue.size() > 0) {
                int tagWeight = Integer.parseInt(tagsWeight.getText());
                String operation = tagsWeightOperation.getSelectedItem().toString();

                QueryBuilder tagsQueryBuilder = new QueryBuilder();
                tagsQueryBuilder.target("MT.movieID")
                        .distinct()
                        .relation("movie_tags MT")
                        .qualification("MT.tagWeight", tagWeight, operation);


                QueryBuilder.andORQuery(isAndOperation, tagsQueryBuilder, queryBuilder, selectedTagsValue,
                        "M.ID", "MT.tagID");
            }

            queryBuilder
                    .target("M.ID")
                    .target("M.Title")
                    .target("M.Year")
                    .target("MCC.country")
                    .target("M.rtAudienceRating", "RT Audience rating")
                    .target("M.rtAudienceNumRatings", "RT Audience number of ratings")
                    .target("M.rtAudienceScore", "RT Audience score")
                    .distinct()
                    .relation("movies M")
                    .relation("movie_countries MCC")
                    .qualification("M.ID", "MCC.movieID")
                    .qualification("M.year", startDate, " >= ")
                    .qualification("M.year", endDate, " <= ")
                    .orderBy("M.Title");

            String finalQuery = queryBuilder.build();

            movieQueryDebugOutput.setText(finalQuery);

            resultSet = sqlUtility.query(finalQuery);
        }

        Map<Object, List> stringObjectMap = new HashMap<>();

        QueryBuilder queryBuilder = new QueryBuilder();

        queryBuilder.target("MG.movieID")
                .target("MG.genre")
                .distinct()
                .relation("movie_genres MG");
        ResultSet resultSet1 = sqlUtility.query(queryBuilder.build());
        try {
            while (resultSet1.next()) {
                if(!stringObjectMap.containsKey(resultSet1.getObject(1))) {
                    stringObjectMap.put(resultSet1.getObject(1), new ArrayList());
                }
                stringObjectMap.get(resultSet1.getObject(1)).add(resultSet1.getObject(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        movieTableModel = new ResultTableModel(resultSet, () -> {
            this.populateUsersTable();
            return 0;
        }, "Genres", stringObjectMap);

        movieResultTable.setModel(movieTableModel);
    }

    private void populateUsersTable() {
        List<Object> selectedMovieIDList = getSelectedValue(movieTableModel);
        List<Object> selectedTagIDList = getSelectedValue(tagsTableModel);

        ResultSet resultSet = null;
        if(selectedMovieIDList.size() > 0 && selectedTagIDList.size() > 0) {
            QueryBuilder queryBuilder = new QueryBuilder();
            QueryBuilder movieQueryBuilder = new QueryBuilder();
            QueryBuilder tagQueryBuilder = new QueryBuilder();

            queryBuilder
                    .target("UT.userID")
                    .distinct()
                    .relation("user_taggedmovies UT");


            movieQueryBuilder.target("M.ID")
                    .distinct()
                    .relation("movies M")
                    .qualificationIN("M.ID", selectedMovieIDList);

            queryBuilder.qualificationIN("UT.movieID", movieQueryBuilder.build());

            int tagWeight = Integer.parseInt(tagsWeight.getText());
            String operation = tagsWeightOperation.getSelectedItem().toString();

            tagQueryBuilder.target("MT.movieID")
                    .distinct()
                    .relation("movie_tags MT")
                    .qualification("MT.tagWeight", tagWeight, operation)
                    .qualificationIN("MT.tagID", selectedTagIDList);

            queryBuilder.qualificationIN("UT.movieID", tagQueryBuilder.build());

            String finalQuery = queryBuilder.build();

            userQueryDebugOutput.setText(finalQuery);

            resultSet = sqlUtility.query(finalQuery);
        }

        userTableModel = new ResultTableModel(resultSet, null);
        userResultTable.setModel(userTableModel);
    }

    private static void createAndShowUI() {
        JFrame frame = new JFrame("COEN 280 HW#3");

        frame.getContentPane().add(new hw3());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(hw3::createAndShowUI);
    }
}

class QueryBuilder implements Cloneable {
    private StringBuilder targetList;
    private StringBuilder relationList;
    private StringBuilder qualifications;
    private StringBuilder orderBy;
    private StringBuilder groupBy;
    private StringBuilder having;

    private boolean needDistinct;

    private String originalSeparator = " AND ";
    private String separator = this.originalSeparator;

    private boolean flip;
    private int startCount;

    QueryBuilder() {
        this.targetList = new StringBuilder();
        this.relationList = new StringBuilder();
        this.qualifications = new StringBuilder();
        this.orderBy = new StringBuilder();
        this.groupBy = new StringBuilder();
        this.having = new StringBuilder();

        this.needDistinct = false;
    }

    QueryBuilder(QueryBuilder queryBuilder) {
        setTargetList(queryBuilder.getTargetList());
        setRelationList(queryBuilder.getRelationList());

        setQualifications(queryBuilder.getQualifications());
        setOrderBy(queryBuilder.getOrderBy());
        setGroupBy(queryBuilder.getGroupBy());

        setHaving(queryBuilder.getHaving());

        setNeedDistinct(queryBuilder.isNeedDistinct());

        setOriginalSeparator(queryBuilder.getOriginalSeparator());
        setSeparator(queryBuilder.getSeparator());

        setFlip(queryBuilder.isFlip());
        setStartCount(queryBuilder.startCount);
    }

    public QueryBuilder distinct() {
        this.needDistinct = true;
        return this;
    }

    public QueryBuilder separator(String separator) {
        this.separator = separator;
        this.flip = true;
        return this;
    }

    public QueryBuilder and() {
        return this.separator(" AND ");
    }

    public QueryBuilder or() {
        return this.separator(" OR ");
    }

    public QueryBuilder target(String target) {
        return this.target(target, null);
    }

    public QueryBuilder target(String target, String as) {
        if(this.targetList.length() > 0) {
            this.targetList.append(", ");
        }
        this.targetList.append(target);
        if(as != null && !"".equalsIgnoreCase(as)) {
            this.targetList.append(" AS ").append("\"").append(as).append("\"");
        }
        return this;
    }

    public QueryBuilder relation(String relation) {
        if(this.relationList.length() > 0) {
            this.relationList.append(", ");
        }
        this.relationList.append(relation);
        return this;
    }

    public QueryBuilder qualification(String qualification, Object value) {
        return this.qualification(qualification, value, " = ");
    }

    public QueryBuilder qualificationANY(String qualification, Object value) {
        return this.qualification(qualification, value, " ANY ");
    }

    public QueryBuilder qualificationIN(String qualification, Object value) {
        return this.qualification(qualification, value, " IN ");
    }

    public QueryBuilder qualificationIN_ANY(String qualification, Object value, boolean isAny) {
        String operation = " IN ";
        if(isAny) {
            operation = " ANY ";
        }
        return this.qualification(qualification, value, operation);
    }

    public QueryBuilder qualificationIN(String qualification, Object value, boolean inAll) {
        return this.qualificationIN(qualification, value, inAll);
    }

    public QueryBuilder qualification(String qualification, Object value, String operation) {
        if(!" = ".equalsIgnoreCase(operation)) {
            StringBuilder tempStringBuilder = new StringBuilder();


            if(value.getClass() == String.class) {
                if(" LIKE ".equalsIgnoreCase(operation)) {
                    tempStringBuilder.append(" \'%");
                    tempStringBuilder.append(value);
                    tempStringBuilder.append("%\' ");
                } else {
                    tempStringBuilder.append(" ( ");
                    tempStringBuilder.append(value);
                    tempStringBuilder.append(" ) ");
                }
            } else if(value.getClass() == Integer.class) {
                tempStringBuilder.append(value);
            } else {
                List value1 = (List) value;
                tempStringBuilder.append(" ( ");
                for(int i = 0; i < value1.size(); i++) {
                    tempStringBuilder.append(QueryBuilder.test(value1.get(i)));

                    if(value1.size() - 1 != i) {
                        tempStringBuilder.append(", ");
                    }
                }
                tempStringBuilder.append(" ) ");
            }

            value = tempStringBuilder.toString();
        }

        if(this.qualifications.length() > 0) {
            this.qualifications.append(separator);
            if(this.flip) {
                this.separator(this.originalSeparator);
                this.flip = false;
            }
        }
        while(this.startCount > 0) {
            this.startCount--;
            this.qualifications.append(" ( ");
        }

        this.qualifications.append(qualification).append(operation).append(value);
        return this;
    }

    public QueryBuilder groupStart() {
        this.startCount++;
        return this;
    }

    public QueryBuilder groupEnd() {
        this.qualifications.append(" ) ");
        return this;
    }

    public QueryBuilder orderBy(String field) {
        return this.orderBy(field, "ASC");
    }

    public QueryBuilder orderBy(String field, String order) {
        if(this.orderBy.length() > 0) {
            this.orderBy.append(", ");
        }
        this.orderBy.append(field).append(" ").append(order);

        return this;
    }

    public QueryBuilder groupBy(String field) {
        if(this.groupBy.length() > 0) {
            this.groupBy.append(", ");
        }
        this.groupBy.append(field);

        return this;
    }

    public QueryBuilder having(String key, String value, String operation) {
        if(this.having.length() > 0) {
            this.having.append(this.separator);
        }
        this.having.append(key);
        this.having.append(operation);
        this.having.append(value);

        return this;
    }

    public QueryBuilder having(String key, String value) {
        return this.having(key, value, " = ");
    }

    public String join(String type, String table, String value1, String value2) {
        return " " + type + " JOIN " + table + " ON " + value1 + " = " + value2 + " ";
    }

    public String build() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("SELECT ");

        if(this.needDistinct) {
            stringBuilder.append("DISTINCT ");
        }

        stringBuilder.append(this.targetList.toString());
        stringBuilder.append(" FROM ");
        stringBuilder.append(this.relationList.toString());

        if(this.qualifications.length() > 0) {
            stringBuilder.append(" WHERE ");
            stringBuilder.append(this.qualifications.toString());
        }
        if(this.groupBy.length() > 0) {
            stringBuilder.append(" GROUP BY ");
            stringBuilder.append(this.groupBy.toString());
            if(this.having.length() > 0) {
                stringBuilder.append(" HAVING ");
                stringBuilder.append(this.having.toString());
            }
        }
        if(this.orderBy.length() > 0) {
            stringBuilder.append(" ORDER BY ");
            stringBuilder.append(this.orderBy.toString());
        }

        return stringBuilder.toString();
    }

    public static Object test(Object value) {
        try {
            if("\\N".equalsIgnoreCase(value.toString())) {
                value = null;
            } else if("infinity".equalsIgnoreCase(value.toString())) {
                throw new Exception();
            } else {
                Float.parseFloat(value.toString());
            }
        } catch (NullPointerException ne) {
            value = null;
        } catch (Exception e) {
            value = "'" + value.toString().replace("'", "''") + "'";
        }

        return value;
    }

    public static void and_or_queries(boolean isAndOperation, List<Object> selectedList, QueryBuilder targetQueryBuilder,
                      String target, String select, String as, String qualification, String table) {
        if(isAndOperation) {
            for(int i = 0; i < selectedList.size(); i++) {
                QueryBuilder countryQueryBuilder = new QueryBuilder();
                countryQueryBuilder.target(as + i + "." + select)
                        .distinct()
                        .relation(table + " " + as + i)
                        .qualification(as + i + "." + qualification, selectedList.get(i));


                targetQueryBuilder.qualificationIN(target, countryQueryBuilder.build());
            }
        } else {
            QueryBuilder countryQueryBuilder = new QueryBuilder();
            countryQueryBuilder.target(as + "." + select)
                    .distinct()
                    .relation(table + " " + as)
                    .qualificationIN(as + "." + select, selectedList);


            targetQueryBuilder.qualificationIN(target, countryQueryBuilder.build());
        }
    }

    public static void andORQuery(boolean isAndOperation,
                                  QueryBuilder selectQueryBuilder,  QueryBuilder targetQueryBuilder,
                                  List selectedList, String target, String as) {
        if(isAndOperation) {
            for(int i = 0; i < selectedList.size(); i++) {
                QueryBuilder queryBuilder = new QueryBuilder(selectQueryBuilder)
                        .qualification(as, QueryBuilder.test(selectedList.get(i)));

                targetQueryBuilder.qualificationIN(target, queryBuilder.build());
            }
        } else {
            QueryBuilder countryQueryBuilder = new QueryBuilder(selectQueryBuilder)
                    .qualificationIN(as, selectedList);
            targetQueryBuilder.qualificationIN(target, countryQueryBuilder.build());
        }
    }

    public StringBuilder getTargetList() {
        return targetList;
    }

    public void setTargetList(StringBuilder targetList) {
        this.targetList = targetList;
    }

    public StringBuilder getRelationList() {
        return relationList;
    }

    public void setRelationList(StringBuilder relationList) {
        this.relationList = relationList;
    }

    public StringBuilder getQualifications() {
        return qualifications;
    }

    public void setQualifications(StringBuilder qualifications) {
        this.qualifications = qualifications;
    }

    public StringBuilder getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(StringBuilder orderBy) {
        this.orderBy = orderBy;
    }

    public StringBuilder getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(StringBuilder groupBy) {
        this.groupBy = groupBy;
    }

    public StringBuilder getHaving() {
        return having;
    }

    public void setHaving(StringBuilder having) {
        this.having = having;
    }

    public boolean isNeedDistinct() {
        return needDistinct;
    }

    public void setNeedDistinct(boolean needDistinct) {
        this.needDistinct = needDistinct;
    }

    public String getOriginalSeparator() {
        return originalSeparator;
    }

    public void setOriginalSeparator(String originalSeparator) {
        this.originalSeparator = originalSeparator;
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public boolean isFlip() {
        return flip;
    }

    public void setFlip(boolean flip) {
        this.flip = flip;
    }

    public int getStartCount() {
        return startCount;
    }

    public void setStartCount(int startCount) {
        this.startCount = startCount;
    }
}

class SQLUtility {
    private String host = "localhost";
    private String port = "1521";
    private String dbName = "ORCLCDB.localdomain";
    private String userName = "mhalinge";
    private String password = "password";

    private Connection connection = null;

    public static void main(String... args) throws SQLException {
        SQLUtility sqlUtility = new SQLUtility();
        QueryBuilder queryBuilder;

        for (String table: populate.fileList) {
            queryBuilder = new QueryBuilder();
            List<List<String>> resultSet = sqlUtility.execute(queryBuilder.target("COUNT(*)").relation(table.replace("-", "_")).build());
            System.out.println("Table: " + table);
            for(List<String> stringList: resultSet) {
                for (String item: stringList) {
                    System.out.print(item + "\t ");
                }
                System.out.println();
            }
        }
    }

    public List<List<String>> execute(String query) {
        Connection con = null;
        try {
            con = openConnection();

            ResultSet resultSet = con.createStatement().executeQuery(query);
            List<List<String>> resultList = new ArrayList<>();

            List<String> temp = new ArrayList<>();
            for (int col = 1; col <= resultSet.getMetaData().getColumnCount(); col++) {
                temp.add(resultSet.getMetaData().getColumnName(col));
            }
            resultList.add(temp);

            while (resultSet.next()) {
                temp = new ArrayList<>();
                for (int col = 1; col <= resultSet.getMetaData().getColumnCount(); col++) {
                    temp.add(resultSet.getObject(col).toString());
                }
                resultList.add(temp);
            }
            return resultList;
        } catch (SQLException e) {
            System.err.println("Errors occurs when communicating with the database server: " + e.getMessage());
        } finally {
            if(con != null) {
                closeConnection(con);
            }
        }

        return null;
    }

    public ResultSet query(String query) {

//        long startTime = new Date().getTime();

        ResultSet resultSet = null;
        try {
            if(!(connection != null && !connection.isClosed())) {
                openConnection();
            }
            resultSet = connection.createStatement().executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

//        System.out.println("Query: \n" + query + "\nTime taken: " + (new Date().getTime() - startTime) + " milliseconds\n");

        return resultSet;
    }

    public Connection openConnection(String host, String port, String dbName, String userName, String password) throws SQLException {
        this.host = host;
        this.port = port;
        this.dbName = dbName;
        this.userName = userName;
        this.password = password;

        return openConnection();
    }

    public Connection openConnection() throws SQLException {
        DriverManager.registerDriver(new OracleDriver());

        String dbURL = "jdbc:oracle:thin:@" + host + ":" + port + "/" + dbName;

        System.out.println(dbURL);

        connection = DriverManager.getConnection(dbURL, userName, password);

        return connection;
    }

    private void closeConnection(Connection con) {
        try {
            con.close();
        } catch (SQLException e) {
            System.err.println("Cannot close connection: " + e.getMessage());
        }
    }

    private void closeConnection() {
        closeConnection(connection);
    }
}

class ResultTableModel extends AbstractTableModel {

    private List<String> columnNames;
    private List<List<Object>> data;

    private Callable function;

    public ResultTableModel(ResultSet resultSet, Callable function) {
        this(resultSet, function, null, null);
    }

    public ResultTableModel(ResultSet resultSet, Callable function, String extraColumn, Map<Object, List> objectListMap) {
//        long startTime = new Date().getTime();

        this.function = function;
        this.columnNames = new ArrayList<>();
        this.data = new ArrayList<>();

        if(resultSet == null) {
            return;
        }
        try {
            if(this.function != null) {
                columnNames.add("");
            }
            for(int col = 1; col <= resultSet.getMetaData().getColumnCount(); col++) {
                this.columnNames.add(resultSet.getMetaData().getColumnLabel(col));
            }

            if(extraColumn != null) {
                this.columnNames.add(extraColumn);
            }


            while(resultSet.next()) {
                List<Object> dataItem = new ArrayList<>();

                if(this.function != null) {
                    dataItem.add(Boolean.FALSE);
                }

                for(int col = 1; col <= resultSet.getMetaData().getColumnCount(); col++) {
                    dataItem.add(resultSet.getObject(col));
                }

                if(extraColumn != null) {
                    dataItem.add(String.join(", ", objectListMap.getOrDefault(resultSet.getObject(1), new ArrayList())));
                } else {
                    dataItem.add("");
                }
                this.data.add(dataItem);
            }
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }

//        System.out.println(this + " Time taken: " + (new Date().getTime() - startTime));
    }

    public int getColumnCount() {
        return columnNames.size();
    }

    public int getRowCount() {
        return data.size();
    }

    public String getColumnName(int col) {
        return columnNames.get(col);
    }

    public Object getValueAt(int row, int col) {
        return data.get(row).get(col);
    }

    public void getRowValueAt(int row) {
        for (int j = 0; j < getColumnCount(); j++) {
            System.out.print("  " + data.get(row).get(j));
        }
        System.out.println();
    }

    public Class getColumnClass(int c) {
        if(getValueAt(0, c) != null)
            return getValueAt(0, c).getClass();
        return "".getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return col <= 1;
    }

    public void setValueAt(Object value, int row, int col) {
        data.get(row).set(col, value);
        fireTableCellUpdated(row, col);

        try {
            if(function != null) {
                function.call();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void printDebugData() {
        int numRows = getRowCount();
        int numCols = getColumnCount();

        for (int i = 0; i < numRows; i++) {
            System.out.print("    row " + i + ":");
            for (int j = 0; j < numCols; j++) {
                System.out.print("  " + data.get(i).get(j));
            }
            System.out.println();
        }
        System.out.println("--------------------------");
    }
}

@SuppressWarnings("serial")
class JCheckBoxList extends JList<JCheckBox> {
    protected static Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);

    public JCheckBoxList() {
        setCellRenderer(new CellRenderer());
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                int index = locationToIndex(e.getPoint());
                if (index != -1) {
                    JCheckBox checkbox = getModel().getElementAt(index);
                    checkbox.setSelected(!checkbox.isSelected());
                    repaint();
                }
            }
        });
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public JCheckBoxList(ListModel<JCheckBox> model){
        this();
        setModel(model);
    }

    protected class CellRenderer implements ListCellRenderer<JCheckBox> {
        public Component getListCellRendererComponent(
                JList<? extends JCheckBox> list, JCheckBox value, int index,
                boolean isSelected, boolean cellHasFocus) {

            //Drawing checkbox, change the appearance here
            value.setBackground(isSelected ? getSelectionBackground()
                    : getBackground());
            value.setForeground(isSelected ? getSelectionForeground()
                    : getForeground());
            value.setEnabled(isEnabled());
            value.setFont(getFont());
            value.setFocusPainted(false);
            value.setBorderPainted(true);
            value.setBorder(isSelected ? UIManager
                    .getBorder("List.focusCellHighlightBorder") : noFocusBorder);
            return value;
        }
    }
}